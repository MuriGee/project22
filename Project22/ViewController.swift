//
//  ViewController.swift
//  Project22
//
//  Created by Muri Gumbodete on 11/04/2022.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

  @IBOutlet var distanceReading: UILabel!
  var locationManager: CLLocationManager?
  var firstBeacon = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    locationManager = CLLocationManager()
    locationManager?.delegate = self
    locationManager?.requestAlwaysAuthorization()
    
    view.backgroundColor = .gray
  }
  
  func startScanning() {
    let uuid = UUID(uuidString: "5A4BCFCE-174E-4BAC-A814-092E77F6B7E5")!
    let beaconRegion = CLBeaconRegion(uuid: uuid, major: 123, minor: 456, identifier: "MyBeacon")
    
    locationManager?.startMonitoring(for: beaconRegion)
    locationManager?.startRangingBeacons(satisfying: CLBeaconIdentityConstraint(uuid: uuid, major: 123, minor: 456))
  }
  
  func update(distance: CLProximity) {
    UIView.animate(withDuration: 1) {
      switch distance {
      case .immediate:
        self.view.backgroundColor = .red
        self.distanceReading.text = "RIGHT HERE"
      case .near:
        self.view.backgroundColor = .orange
        self.distanceReading.text = "NEAR"
      case .far:
        self.view.backgroundColor = .blue
        self.distanceReading.text = "FAR"
      default:
        self.view.backgroundColor = .gray
        self.distanceReading.text = "UNKNOWN"
      }
    }
  }
}

extension ViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    if status == .authorizedAlways {
      if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
        if CLLocationManager.isRangingAvailable() {
          startScanning()
        }
      }
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didRange beacons: [CLBeacon], satisfying beaconConstraint: CLBeaconIdentityConstraint) {
    if let beacon = beacons.first {
      update(distance: beacon.proximity)
      if firstBeacon == false {
        let ac = UIAlertController(title: "Beacon Found", message: "We have found a beacon in the proximity", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default) {
          action in
          self.firstBeacon = true
        })
        present(ac, animated: true)
      }
    } else {
      update(distance: .unknown)
    }
  }
}
